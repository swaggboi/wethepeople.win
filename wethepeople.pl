#!/usr/bin/env perl

use Mojolicious::Lite;

get '/' => sub {
    my ($c) = @_;

    $c->redirect_to('/welcome');
};

get '/*' => sub {
    my ($c) = @_;

    $c->render();
} => 'index';

app->start();
