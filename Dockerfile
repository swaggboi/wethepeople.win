FROM docker.io/perl:5.32

# Move it
WORKDIR /opt
COPY public/ ./public/
COPY templates/ ./templates/
COPY wethepeople.pl .

# Upgrade
RUN apt-get update
RUN apt-get -y upgrade

# Install Mojo
RUN cpanm Mojolicious

# Set up environment
ENV MOJO_REVERSE_PROXY=1
EXPOSE 3000

# Send it
CMD ["perl", "wethepeople.pl", "prefork", "-m", "production"]
