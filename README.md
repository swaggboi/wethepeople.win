# wethepeople.win

Source code for http://wethepeople.win

## Building/Testing

### Build the container locally

    docker build -t wethepeople.pl .

### Run it locally

    docker run -p 3004:3000 wethepeople.pl

Pull up [http://localhost:3004](http://localhost:3004) and poke around

## Tagging/Pushing

### Tag the image

    docker tag wethepeople.pl \
        git.seriousbusiness.international/swaggboi_priv/wethepeople.pl

### Send it

    docker push git.seriousbusiness.international/swaggboi_priv/wethepeople.pl

### Pull

    podman pull git.seriousbusiness.international/swaggboi_priv/wethepeople.pl

### Run

    podman run -dt --rm --name wethepeople.pl -p 3004:3000 wethepeople.pl

### Unit file

    podman generate systemd --files --new --name wethepeople.pl
